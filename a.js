const { sub } = require('date-fns');
const fs = require('fs');

const now = new Date();
const random = (max) => Math.round(Math.random() * max);
const randomBoolean = () => !!random(1);
const contribution = () => random(1000);
const drop = () => random(300);

let value = 0;

const investiments = Array(3653)
  .fill(null)
  .map((_, index) => sub(now, { days: index }).getTime())
  .reverse()
  .map((time) => {
    value = randomBoolean() ? value += contribution() : value -= drop();

    return [time, value];
  });

fs.writeFile('./data.json', JSON.stringify(investiments), (error) => {
  if (error) {
    return console.error(error);
  }

  console.log('Graph data was created');
});
