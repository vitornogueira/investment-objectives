# Investiments

## Dependências

- [Node.js](https://nodejs.org/en/)
- [yarn](https://legacy.yarnpkg.com/lang/en/)

## Rodando o projeto

Instale as dependências utilizando o comando `yarn install` e depois rode o comando `yarn start`.

## Testes e lint

Utilize o comando `yarn test` para rodar os testes unitários e o comando `yarn lint` para validar as regras de estilo do código.

## Deploy

Para realizar o deploy basta dar um push para a branch master do repositório. O [pipeline](https://gitlab.com/vitornogueira/investment-objectives/pipelines) executará os processos de teste, lint, build e deploy.

## Estrutura da solução

- components
	- Componentes utilizados para montar as telas (screens)
- screens
	- Componentes utilizados nas rotas (conjunto de components)
- services
	- Módulos para abstrair a camada de negócio provendo métodos utilizados pelos componentes

## Planejamento da solução

Como eu trabalho com Vuejs eu iniciei criando uma POC de react + gráficos para estudar os pontos principais do framework e das bibliotecas de gráficos dísponiveis. Testei 3 bibliotecas para gráficos:

- [Recharts](http://recharts.org/en-US)
- [Nivo](https://nivo.rocks/)
- [react-timeseries-charts](https://github.com/esnet/react-timeseries-charts)

Escolhei a Recharts por conta da sua documentação e exemplos.

A solução foi criada utilizando o [create-react-app](https://github.com/facebook/create-react-app), para estilização dos componentes eu optei por utilizar a biblioteca [styled-components](https://styled-components.com/) (eu já havia utilizado em meus estudos recentes sobre [react-native](https://facebook.github.io/react-native/)).

Listei os assuntos que eu precisava estudar para atender os requisitos requisitos e planejei a estrutura da aplicação.

Dividi a solução em duas partes:

- Tela de exibição dos investimentos para atender o que foi solicitado;
- Tela com listagem de objetivos e a exibição de detalhes de cada objetivo;

Usei como base para estilização do `select` com os filtros de período para exibição do gráfico esse exemplo: [Styling a Select Like It’s 2019 - https://css-tricks.com/styling-a-select-like-its-2019/](https://css-tricks.com/styling-a-select-like-its-2019/).

## Fontes de estudo:

- https://css-tricks.com/accessible-svgs/
- https://css-tricks.com/styling-a-select-like-its-2019/
- https://blog.bitsrc.io/top-5-react-chart-libraries-for-2020-194acc5a15cf
- https://medium.com/@samuelsetsoafia/nivo-a-great-alternative-to-d3-in-react-6cb18d907d2
- https://www.youtube.com/watch?v=rhNXICPh_ts
- https://dev.to/sidhantpanda/self-hosted-gitlab-continuous-deployment-to-netlify-49lk
- https://medium.com/@am9/deploy-your-static-site-to-netlify-using-gitlab-ci-e30ff3a81398
- https://create-react-app.dev/docs/running-tests
- https://create-react-app.dev/docs/deployment/
- https://levelup.gitconnected.com/styled-components-essentials-in-three-steps-a61fb9372ded
- https://medium.com/@rishabhsrao/mocking-and-testing-fetch-with-jest-c4d670e2e167
- https://stackoverflow.com/a/58525708
- https://medium.com/js-dojo/deploying-vue-js-to-netlify-using-gitlab-continuous-integration-pipeline-1529a2bbf170
- https://github.com/testing-library/react-hooks-testing-library
- https://kentcdodds.com/blog/react-hooks-whats-going-to-happen-to-my-tests
- https://www.carnaghan.com/knowledge-base/how-to-center-a-image-in-gimp-2-8/
- https://www.youtube.com/watch?v=xtMWpDH0z6w

Favicon made by [Freepik](https://www.flaticon.com/authors/freepik) from [www.flaticon.com](http://www.flaticon.com/)
