import React from 'react';
import ReactDOM from 'react-dom';
import Index from './screens/Index';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<Index />, document.getElementById('root'));

serviceWorker.register();
