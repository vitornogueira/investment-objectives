import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

import GlobalStyle from './styles';

import Objectives from '../Objectives';
import Objective from '../Objective';
import Dashboard from '../../components/Dashboard';

function Index() {
  return (
    <Router>
      <GlobalStyle />

      <Dashboard>
        <Switch>
          <Route path="/objectives/:objectiveId">
            <Objective />
          </Route>
          <Route path="/">
            <Objectives />
          </Route>
        </Switch>
      </Dashboard>
    </Router>
  );
}

export default Index;
