import React, { useState, useEffect } from 'react';
import { useParams, NavLink } from 'react-router-dom';

import Title from '../../components/Title';
import Filter from '../../components/Filter';
import Chart from '../../components/Chart';
import Storage from '../../services/storage';
import useStorageFilter from '../../hooks/useStorageFilter';
import api from '../../services/api';

const objectivesTitles = {
  1: 'Intercâmbio',
  2: 'Aposentadoria',
};

function Objectives() {
  const storage = new Storage({ key: 'investiments.objectives.time.filter' });
  const { filter, saveFilter } = useStorageFilter(storage);
  const [sourceData, setSourceData] = useState([]);
  const { objectiveId } = useParams();

  const handleChangeFilter = (selectedFilter) => saveFilter(selectedFilter);

  useEffect(() => {
    (async () => {
      const responseData = await api.get({ objectiveId });

      setSourceData(responseData);
    })();
  }, [objectiveId]);

  return (
    <>
      <Title>
        <NavLink to="/">Objetivos</NavLink>
        <span>{` / ${objectivesTitles[objectiveId]}`}</span>
      </Title>
      <Filter onChange={handleChangeFilter} storage={storage} />
      <Chart filter={filter} sourceData={sourceData} />
    </>
  );
}

export default Objectives;
