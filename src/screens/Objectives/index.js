import React from 'react';
import { NavLink } from 'react-router-dom';

import Title from '../../components/Title';
import Widget, { WidgetTitle, WidgetValue } from '../../components/Widget';
import { money } from '../../services/formatter';

function Home() {
  return (
    <>
      <Title>Meus objetivos</Title>
      <Widget>
        <NavLink to="/objectives/2">
          <WidgetTitle>aposentadoria</WidgetTitle>
          <WidgetValue>{ money({ value: 636913 }) }</WidgetValue>
        </NavLink>
      </Widget>
      <Widget>
        <NavLink to="/objectives/1">
          <WidgetTitle>Intercâmbio</WidgetTitle>
          <WidgetValue>{ money({ value: 138880.49 }) }</WidgetValue>
        </NavLink>
      </Widget>
    </>
  );
}

export default Home;
