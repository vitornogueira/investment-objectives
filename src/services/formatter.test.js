import { date, money, graphData } from './formatter';

describe('services/formatter', () => {
  describe('date', () => {
    let datetime;
    let dateString;

    describe('when only timestamp is provided', () => {
      beforeEach(() => {
        datetime = new Date();

        dateString = date({ timestamp: datetime.getTime() });
      });

      test('return date with pt-BR format and short style', () => {
        expect(dateString).toEqual(datetime.toLocaleString('pt-BR', { dateStyle: 'short' }));
      });
    });

    describe('when timestamp, language and options are provided', () => {
      beforeEach(() => {
        datetime = new Date();

        dateString = date({
          timestamp: datetime.getTime(),
          language: 'en-US',
          options: { dateStyle: 'long' },
        });
      });

      test('return date with language and options provided', () => {
        expect(dateString).toEqual(datetime.toLocaleString('en-US', { dateStyle: 'long' }));
      });
    });
  });

  describe('money', () => {
    let valueFormated;

    describe('when only value is provided', () => {
      beforeEach(() => {
        valueFormated = money({ value: 1000 });
      });

      test('return value with pt-BR language and BRL currency', () => {
        expect(valueFormated).toEqual((1000).toLocaleString('pt-BR', {
          style: 'currency',
          currency: 'BRL',
        }));
      });
    });

    describe('when value, language and currency are provided', () => {
      beforeEach(() => {
        valueFormated = money({
          value: 1000,
          language: 'es-UY',
          currency: 'URU',
        });
      });

      test('return value with pt-BR language and BRL currency', () => {
        expect(valueFormated).toEqual((1000).toLocaleString('es-UY', {
          style: 'currency',
          currency: 'URU',
        }));
      });
    });
  });

  describe('graphData', () => {
    let sourceData;
    let transformedData;

    beforeEach(() => {
      sourceData = [
        [new Date(2020, 0, 16), 116],
        [new Date(2020, 0, 17), 117],
        [new Date(2020, 0, 18), 118],
        [new Date(2020, 0, 19), 119],
        [new Date(2020, 0, 20), 120],
      ];
    });

    describe('when filter.key is "all"', () => {
      beforeEach(() => {
        transformedData = graphData({
          data: sourceData,
          filter: { key: 'all' },
        });
      });

      test('transform all data', () => {
        expect(transformedData).toEqual([
          { time: new Date(2020, 0, 16), amount: 116 },
          { time: new Date(2020, 0, 17), amount: 117 },
          { time: new Date(2020, 0, 18), amount: 118 },
          { time: new Date(2020, 0, 19), amount: 119 },
          { time: new Date(2020, 0, 20), amount: 120 },
        ]);
      });
    });

    describe('when filter.key is not "all"', () => {
      beforeEach(() => {
        transformedData = graphData({
          data: sourceData,
          filter: { key: 'last_2_days', time: new Date(2020, 0, 18) },
        });
      });

      test('filter data by time and transform data', () => {
        expect(transformedData).toEqual([
          { time: new Date(2020, 0, 19), amount: 119 },
          { time: new Date(2020, 0, 20), amount: 120 },
        ]);
      });
    });
  });
});
