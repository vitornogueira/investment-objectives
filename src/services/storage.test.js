import Storage from './storage';

describe('services/storage', () => {
  let storage;

  describe('store', () => {
    describe('when no store is provided', () => {
      beforeEach(() => {
        storage = new Storage({ key: 'key' });
      });

      test('set store equal localStorage', () => {
        expect(storage.store).toEqual(global.localStorage);
      });
    });

    describe('when store is provided', () => {
      beforeEach(() => {
        storage = new Storage({ key: 'key', store: global.sessionStorage });
      });

      test('set store equal store provided', () => {
        expect(storage.store).toEqual(global.sessionStorage);
      });
    });
  });

  describe('set', () => {
    beforeEach(() => {
      storage = new Storage({ key: 'key', store: { setItem: jest.fn() } });

      jest.spyOn(storage.store, 'setItem');

      storage.set({ value: 'value' });
    });

    test('call storage.store.setItem with data provided', () => {
      expect(storage.store.setItem).toHaveBeenCalledWith(
        'key',
        JSON.stringify({ value: 'value' }),
      );
    });
  });

  describe('get', () => {
    beforeEach(() => {
      storage = new Storage({ key: 'key' });
    });

    describe('when there is no saved data', () => {
      test('return null', () => {
        expect(storage.get()).toBe(null);
      });
    });

    describe('when there is some saved data', () => {
      beforeEach(() => {
        storage.set({ value: 'value' });
      });

      test('return stored data', () => {
        expect(storage.get()).toEqual({ value: 'value' });
      });
    });
  });
});
