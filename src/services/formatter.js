export const date = ({
  timestamp,
  language = 'pt-br',
  options = { dateStyle: 'short' },
}) => (new Date(timestamp)).toLocaleString(language, options);

export const money = ({
  value = 0,
  language = 'pt-br',
  currency = 'BRL',
}) => value.toLocaleString(language, { style: 'currency', currency });

export const graphData = ({
  data = [],
  filter = {},
}) => {
  let newData = data;

  if (filter.key !== 'all') {
    newData = data.filter(([time]) => time > filter.time);
  }

  return newData.map(([time, amount]) => ({ time, amount }));
};
