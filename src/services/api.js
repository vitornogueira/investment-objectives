const api = {};

const objectives = {
  1: 'https://gist.githubusercontent.com/AgtLucas/a67c345e15c2eb3d4668c9b7e330ac44/raw/1de2450cbe69fde065bca9e498aaaaafcca61257/mock-data.js',
  2: 'https://gist.githubusercontent.com/vitornogueira/a402dfb03f239474766b57b54fece16c/raw/be07f6cdd1c813fb9461cb30a792efc9d02a8e93/data.json',
}

api.get = async ({ objectiveId = 1 }) => {
  const response = await fetch(objectives[objectiveId]);
  const data = await response.json();

  return data;
};

export default api;
