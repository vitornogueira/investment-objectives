export default class Storage {
  constructor({ key = '', store = localStorage }) {
    this.key = key;
    this.store = store;
  }

  get() {
    return JSON.parse(this.store.getItem(this.key));
  }

  set(value = {}) {
    this.store.setItem(this.key, JSON.stringify(value));
  }
}
