import api from './api';

describe('services/api', () => {
  describe('get', () => {
    describe('when call with objective id equal 1', () => {
      beforeEach(() => {
        jest.spyOn(global, 'fetch');

        api.get({ objectiveId: 1 });
      });

      test('call fetch with first endpoint to json', () => {
        expect(global.fetch).toHaveBeenCalledWith('https://gist.githubusercontent.com/AgtLucas/a67c345e15c2eb3d4668c9b7e330ac44/raw/1de2450cbe69fde065bca9e498aaaaafcca61257/mock-data.js');
      });
    });

    describe('when call with objective id equal 2', () => {
      beforeEach(() => {
        jest.spyOn(global, 'fetch');

        api.get({ objectiveId: 2 });
      });

      test('call fetch with first endpoint to json', () => {
        expect(global.fetch).toHaveBeenCalledWith('https://gist.githubusercontent.com/vitornogueira/a402dfb03f239474766b57b54fece16c/raw/be07f6cdd1c813fb9461cb30a792efc9d02a8e93/data.json');
      });
    });
  });
});
