import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import lolex from 'lolex';
import { sub } from 'date-fns';
import Filter from './index';
import Storage from '../../services/storage';

describe('components/Filter', () => {
  let clock;
  let now;
  let handleChange;
  let select;
  let storage;

  beforeEach(() => {
    clock = lolex.install();

    clock.tick(1579 * 1000 * 1000 * 1000);

    now = new Date();

    handleChange = jest.fn();
    storage = new Storage({ key: 'storage.key' });
  });

  afterEach(() => {
    clock = clock.uninstall();
  });

  describe('when component is rendered', () => {
    describe('and does not have any filter in storage', () => {
      beforeEach(() => {
        jest.spyOn(storage, 'get').mockReturnValue(null);

        render(<Filter onChange={handleChange} storage={storage} />);

        select = screen.getByRole('listbox');
      });

      test('set select value equal default value', () => {
        expect(select.value).toEqual('all');
      });
    });

    describe('and has any filter in storage', () => {
      beforeEach(() => {
        jest.spyOn(storage, 'get').mockReturnValue({ key: 'last_1_year' });

        render(<Filter onChange={handleChange} storage={storage} />);

        select = screen.getByRole('listbox');
      });

      test('set select value equal filter key storaged', () => {
        expect(select.value).toEqual('last_1_year');
      });
    });
  });


  describe('when user change selected filter', () => {
    let expectedFilter;

    beforeEach(() => {
      jest.spyOn(storage, 'set');

      render(<Filter onChange={handleChange} storage={storage} />);

      select = screen.getByRole('listbox');
    });

    describe('and value selected is all', () => {
      beforeEach(() => {
        expectedFilter = { time: undefined, key: 'all' };

        fireEvent.change(select);
      });

      test('set filter on storage', () => {
        expect(storage.set).toHaveBeenCalledWith(expectedFilter);
      });

      test('call onChange callback without time and "all" key', () => {
        expect(handleChange).toHaveBeenCalledWith(expectedFilter);
      });
    });

    describe('and value selected is last one month', () => {
      beforeEach(() => {
        expectedFilter = {
          time: sub(now, { months: 1 }).getTime(),
          key: 'last_1_month',
        };

        fireEvent.change(select, { target: { value: 'last_1_month' } });
      });

      test('set filter on storage', () => {
        expect(storage.set).toHaveBeenCalledWith(expectedFilter);
      });

      test('call onChange callback with time equal one month ago and "last_1_month" key', () => {
        expect(handleChange).toHaveBeenCalledWith(expectedFilter);
      });
    });

    describe('and value selected is last three months', () => {
      beforeEach(() => {
        expectedFilter = {
          time: sub(now, { months: 3 }).getTime(),
          key: 'last_3_months',
        };

        fireEvent.change(select, { target: { value: 'last_3_months' } });
      });

      test('set filter on storage', () => {
        expect(storage.set).toHaveBeenCalledWith(expectedFilter);
      });

      test('call onChange callback with time equal three month ago and "last_3_month" key', () => {
        expect(handleChange).toHaveBeenCalledWith(expectedFilter);
      });
    });

    describe('and value selected is last one year', () => {
      beforeEach(() => {
        expectedFilter = {
          time: sub(now, { years: 1 }).getTime(),
          key: 'last_1_year',
        };

        fireEvent.change(select, { target: { value: 'last_1_year' } });
      });

      test('set filter on storage', () => {
        expect(storage.set).toHaveBeenCalledWith(expectedFilter);
      });

      test('call onChange callback with time equal one year ago and "last_1_year" key', () => {
        expect(handleChange).toHaveBeenCalledWith(expectedFilter);
      });
    });

    describe('and value selected is last two years', () => {
      beforeEach(() => {
        expectedFilter = {
          time: sub(now, { years: 2 }).getTime(),
          key: 'last_2_years',
        };

        fireEvent.change(select, { target: { value: 'last_2_years' } });
      });

      test('set filter on storage', () => {
        expect(storage.set).toHaveBeenCalledWith(expectedFilter);
      });

      test('call onChange callback with time equal two years ago and "last_2_years" key', () => {
        expect(handleChange).toHaveBeenCalledWith(expectedFilter);
      });
    });
  });
});
