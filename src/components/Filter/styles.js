import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  padding: 12px 16px;
  border-radius: 8px;
  background: #f6f7f9;
  color: #597183;
`;

export const Message = styled.p`
  margin: 0 8px;
`;

export const Select = styled.select`
  display: block;
  margin: 0;
  padding: 4px 28px 4px 16px;
  border: 1px solid #aaa;
  border-radius: 24px;
  box-shadow: 0 1px 0 1px rgba(0,0,0,.04);
  background-color: #f6f7f9;
  background-image: url('data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22292.4%22%20height%3D%22292.4%22%3E%3Cpath%20fill%3D%22%23007CB2%22%20d%3D%22M287%2069.4a17.6%2017.6%200%200%200-13-5.4H18.4c-5%200-9.3%201.8-12.9%205.4A17.6%2017.6%200%200%200%200%2082.2c0%205%201.8%209.3%205.4%2012.9l128%20127.9c3.6%203.6%207.8%205.4%2012.8%205.4s9.2-1.8%2012.8-5.4L287%2095c3.5-3.5%205.4-7.8%205.4-12.8%200-5-1.9-9.2-5.5-12.8z%22%2F%3E%3C%2Fsvg%3E');
  background-position: right .7em top 50%, 0 0;
  background-repeat: no-repeat, repeat;
  background-size: .65em auto, 100%;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  font-family: Poppins, sans-serif;
  font-size: 16px;
  font-weight: 600;
  line-height: 1.3;
  color: #597183;

  &::-ms-expand {
    display: none;
  }

  &:hover {
    border-color: #888;
  }

  &:focus {
    border-color: #aaa;
    box-shadow: 0 0 1px 2px rgba(21, 156, 228, .5);
    box-shadow: 0 0 0 2px -moz-mac-focusring;
    color: #222;
    outline: none;
  }

  option {
    font-weight: normal;
  }
`;
