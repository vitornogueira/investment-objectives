import React from 'react';
import { MdDateRange } from 'react-icons/md';
import { sub } from 'date-fns';

import { Container, Message, Select } from './styles';

function Filter({ onChange, storage = {} }) {
  const now = new Date();
  const value = storage.get() || {};

  const filters = {
    last_1_month: sub(now, { months: 1 }).getTime(),
    last_3_months: sub(now, { months: 3 }).getTime(),
    last_1_year: sub(now, { years: 1 }).getTime(),
    last_2_years: sub(now, { years: 2 }).getTime(),
  };

  const handleChange = (event) => {
    const key = event.target.value;
    const time = filters[key];
    const filter = { time, key };

    storage.set(filter);

    onChange(filter);
  };

  return (
    <Container>
      <MdDateRange size={36} title="Ícone de calendário" role="img" />
      <Message>Você está vendo seus investimentos</Message>
      <Select value={value.key} aria-label="Filtro de período" onChange={handleChange}>
        <option value="all">desde o início</option>
        <option value="last_1_month">do último mês</option>
        <option value="last_3_months">dos 3 últimos meses</option>
        <option value="last_1_year">do último ano</option>
        <option value="last_2_years">dos 2 últimos anos</option>
      </Select>
    </Container>
  );
}

export default Filter;
