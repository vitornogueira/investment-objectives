import React from 'react';

import { Container, Footer } from './styles';

function Dashboard({ children }) {
  return (
    <Container>
      <section>{children}</section>
      <Footer>
        <span>Favicon made by </span>
        <a href="https://www.flaticon.com/authors/freepik" title="Freepik" target="_blank" rel="noopener noreferrer">Freepik</a>
        <span> from </span>
        <a href="https://www.flaticon.com/" title="Flaticon" target="_blank" rel="noopener noreferrer">www.flaticon.com</a>
      </Footer>
    </Container>
  );
}

export default Dashboard;
