import styled from 'styled-components';

export const Container = styled.main`
  width: 95%;
  max-width: 980px;
  margin: 0 auto;
  padding-top: 32px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const Footer = styled.footer`
  padding: 16px;
  font-family: Lato, sans-serif;
  font-size: 12px;
  text-align: right;
  color: #8296a4;

  a {
    color: #1663a0;
  }
`;
