import React, { useState, useEffect } from 'react';
import {
  ResponsiveContainer, AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip,
} from 'recharts';

import Container from './styles';
import { date, money, graphData } from '../../services/formatter';

function Chart({ filter, sourceData }) {
  const [data, setData] = useState([]);

  useEffect(() => {
    setData(graphData({ data: sourceData, filter }));
  }, [filter, sourceData]);

  return (
    <Container>
      <ResponsiveContainer width="100%" aspect={2.5}>
        <AreaChart
          data={data.slice()}
          stackOffset="expand"
          margin={{
            top: 20, right: 50, left: 70, bottom: 20,
          }}
        >
          <CartesianGrid strokeDasharray="1 0" vertical={false} />
          <XAxis
            type="number"
            dataKey="time"
            tickCount={8}
            tickMargin={15}
            tickFormatter={(timestamp) => date({ timestamp })}
            domain={['dataMin', 'dataMax']}
          />
          <YAxis
            orientation="right"
            padding={{ top: 20 }}
            tickFormatter={(value) => money({ value })}
            domain={[(dataMin) => dataMin - 5000, 'dataMax']}
          />
          <Tooltip
            labelFormatter={(timestamp) => date({ timestamp })}
            formatter={(value) => [money({ value }), 'Valor']}
          />
          <Area type="linear" dataKey="amount" stroke="#159ce4" fill="#159ce4" />
        </AreaChart>
      </ResponsiveContainer>
    </Container>
  );
}

export default Chart;
