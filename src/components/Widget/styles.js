import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 8px;
  padding: 16px;
  border: 1px solid #e9eef2;
  background: #f6f7f8;
  border-radius: 16px;
  font-weight: 600;

  a {
    text-decoration: none;
  }
`;

export const Title = styled.p`
  color: #597183;
  margin-top: 0px;
  margin-bottom: 4px;
`;

export const Value = styled.p`
  margin: 0;
  font-size: 48px;
  color: #32383c;
`;
