import React from 'react';

import { Container, Title, Value } from './styles';

function Widget({ children }) {
  return <Container>{children}</Container>;
}

export function WidgetTitle({ children }) {
  return <Title>{children}</Title>;
}

export function WidgetValue({ children }) {
  return <Value>{children}</Value>;
}

export default Widget;
