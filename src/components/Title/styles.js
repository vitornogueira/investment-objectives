import styled from 'styled-components';

export default styled.h1`
  margin: 0 0 8px;
  color: #597183;

  a {
    text-decoration: none;
    color: #159ce4;
  }
`;
