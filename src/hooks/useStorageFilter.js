import { useState } from 'react';

export default function useStorageFilter(storage) {
  const currentFilter = storage.get() || { time: 0, key: 'all' };
  const [filter, setFilter] = useState(currentFilter);

  const saveFilter = (value) => setFilter(value);

  return { filter, saveFilter };
}
