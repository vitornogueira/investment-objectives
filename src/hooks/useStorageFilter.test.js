import { renderHook, act } from '@testing-library/react-hooks';

import useStorageFilter from './useStorageFilter';

describe('hooks/useStorageFilter', () => {
  let result;

  describe('when there is no filter saved in storage', () => {
    beforeEach(() => {
      const storage = { get: () => null };

      ({ result } = renderHook(() => useStorageFilter(storage)));
    });

    test('return default filter', () => {
      expect(result.current.filter).toEqual({ key: 'all', time: 0 });
    });
  });

  describe('when there is filter saved in storage', () => {
    beforeEach(() => {
      const storage = { get: () => ({ key: 'last_1_month', time: 1234 }) };

      ({ result } = renderHook(() => useStorageFilter(storage)));
    });

    test('return filter saved in storage', () => {
      expect(result.current.filter).toEqual({ key: 'last_1_month', time: 1234 });
    });
  });

  describe('when saveFilter is called', () => {
    beforeEach(() => {
      const storage = { get: () => null };

      ({ result } = renderHook(() => useStorageFilter(storage)));

      act(() => {
        result.current.saveFilter({ key: 'last_3_years', time: 321 });
      });
    });

    test('return filter provided in saveFilter', () => {
      expect(result.current.filter).toEqual({ key: 'last_3_years', time: 321 });
    });
  });
});
